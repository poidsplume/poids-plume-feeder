#!/bin/bash
# 
# This script is run by Gitlab CI on commit push
# 

# Check bash scripts
find . -type f -not -path '*/\.*' > .filelist.tmp
STATUS=0

while read -r f; do 
  if [ "$(head -n 1 "$f" | grep -c '#!/bin/bash')" -gt 0 ]; then 
    echo "Checking $f..."
    if ! shellcheck -S warning "$f"; then 
      STATUS=1;
    fi 
  fi
done < .filelist.tmp



# Check C files
find . -type f -name "*.c" -not -path '*/\.*' > .filelist.tmp

while read -r f; do 
  echo "Checking $f..."
  
  if cppcheck --error-exitcode=1 "$f"; then 
    STATUS=1;
  fi 
done < .filelist.tmp



if [ $STATUS -gt 0 ]; then 
  echo "Some checks failed !"
fi

rm .filelist.tmp
exit $STATUS
