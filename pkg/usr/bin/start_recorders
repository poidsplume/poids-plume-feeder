#!/bin/bash
# 
#     Copyright (2019), the Poids-Plume project (Alexandre Génin)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# start_recorders 
# ***************
# 
# This script will launch smaller scripts that record data with the desired
# frequency. 
# 

# Load parameters
source /etc/feeder/feeder.conf

export WLOG="startup.log"

# We launch a recorder for each file 
for datafile in "$LOGDIR"/*; do 
  
  basedatfile=$(basename "$datafile")
  case $basedatfile in 
    weightav) 
      PERIOD_LOG=$PERIOD_LOG_MED;; 
    weight*) # mind the *, so it also works for weight0, weight1... 
             # but not weightav because it is handled on the line above
      PERIOD_LOG=$PERIOD_LOG_FAST;; 
    loadavg|systemp|diskspace|sshtime|wifilnk)
      PERIOD_LOG=$PERIOD_LOG_MED;; 
    env*) 
      PERIOD_LOG=$PERIOD_LOG_SLOW;; 
    *) 
      echo "NOTE: using default period (${PERIOD_LOG_DEFAULT}s) for" \
        "datafile $basedatfile !" | wlog
      PERIOD_LOG=$PERIOD_LOG_DEFAULT;;
  esac
  
  outfile="$(basename "$datafile").dat"
  record_data "$datafile" "$outfile" "$PERIOD_LOG" & 
  echo "Recording $datafile as $outfile every ${PERIOD_LOG}s (PID:$!)"
  
done
  
# This loop will keep the script hanging indefinitely. Otherwise the script 
# exits and subjobs are killed. We could also add a nohup to the code above 
# I suppose. 
while true; do 
  sleep 3600
done
