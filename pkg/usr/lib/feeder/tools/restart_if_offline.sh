#!/bin/bash
# 
# Check if there is an active WiFi connection, and if not, restart the feeder. 
# 

source /etc/feeder/feeder.conf

export WLOG="system.log"

if iwgetid -r; then 
  echo "We are online - not rebooting" | wlog
else 
  echo "Rebooting because we are offline" | wlog
  reboot
fi

