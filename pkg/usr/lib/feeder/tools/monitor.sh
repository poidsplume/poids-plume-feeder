#!/bin/bash
# 

# Load parameters 
source "/etc/feeder/feeder.conf"

echo "$(hostname) - $(date)"
echo "-----------" 
echo "" 

TMPFILE="/tmp/monitor.dat"
echo -n > "/tmp/monitor.dat"
# shellcheck disable=SC2231
for i in ${LOGDIR}/*; do  
  echo "$(basename "$i"):$(cat "$i")" >> "$TMPFILE"
done

column -t -s":" "$TMPFILE"

