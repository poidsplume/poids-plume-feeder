#
# This file contains functions that can be executed 
# 

# Get a file to log to, taking the date into account
function dlog { 
  
  DESTDIR="$RAWDIR/$(date +%Y/%m/%d/%H)"
  DESTFILE="$DESTDIR/$1"
  
  # Create dir if needed 
  if [ ! -d "$DESTDIR" ]; then 
    mkdir -p $DESTDIR
  fi

  # Create file if needed
  if [ ! -f "$DESTFILE" ]; then
    touch "$DESTFILE"
  fi
  
  echo "$DESTFILE"
}

# This function assumes $WLOG in the environment, which is the name 
# of the log file. 
function wlog { 
  
  if [ -z "$WLOG" ]; then
    echo "\$WLOG is undefined! Exiting"
    exit 1
  fi
  
  DESTFILE="$(dlog $WLOG)"
  
  curdate="$(date +%s.%N)"
  while read line; do 
    echo "$curdate: $line" >> "$DESTFILE"
  done
}

