/*
 *     Copyright (2019), the Poids-Plume project (Alexandre Génin)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/* 
 * This code is derived from the wiringPi adapation of gurov's code 
 * available there: https://github.com/ggurov/hx711
 */

/* 
 * gurov was here, use this code, or don't, whatever, I don't care. If 
 * you see a giant bug with a billion legs, please let me know so it can 
 * be squashed
*/

#include <stdint.h>
#include <sys/types.h>

#include <stdio.h>
#include <sched.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <math.h>

// Time between samples (microsecond)
#include <unistd.h>

// Parameters
const int data_pin0  = HX711_0_PIN;
const int clock_pin0 = HX711_0_CLK;

#if BI_HX711
const int data_pin1  = HX711_1_PIN;
const int clock_pin1 = HX711_1_CLK;
#endif 

const int RESET_EVERY   = 500; 

const int N_RAW_POINTS  = 20; 
// 
// Function that are common to all hx711 readers
// 

void setHighPri (void) {
  struct sched_param sched;
  memset (&sched, 0, sizeof(sched)) ;
  
  sched.sched_priority = 10 ;
  if ( sched_setscheduler(0, SCHED_FIFO, &sched) ) { 
    printf("Warning: Unable to set high priority\n");
  }
}

static uint8_t sizecvt(const int read) {
  /* digitalRead() and friends from wiringpi are defined as returning a 
    * value < 256. However, they are returned as int() types. This is a 
    * safety function 
    */
  if (read > 255 || read < 0) {
      printf("Invalid data from wiringPi library\n");
      exit(EXIT_FAILURE);
  }
  return (uint8_t)read;
}

void power_down_hx711(int clock_pin) {
  digitalWrite(clock_pin, HIGH);
}

void setup_gpio(int clock_pin, int data_pin) {
  pinMode(clock_pin, OUTPUT);
  pinMode(data_pin, INPUT);
  digitalWrite(clock_pin, LOW);
}

void reset_converter(int clock_pin) {
  digitalWrite(clock_pin, HIGH);
  delayMicroseconds(60);
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(60);
}

void set_gain(int r, int clock_pin, int data_pin) {
  int i;

  // r = 0 - 128 gain ch a
  // r = 1 - 32  gain ch b
  // r = 2 - 63  gain ch a

  while( sizecvt(digitalRead(data_pin)) ) { 
  };

  for (i=0;i<24+r;i++) {
    digitalWrite(clock_pin, HIGH);
    delayMicroseconds(1);
    digitalWrite(clock_pin, LOW);
    delayMicroseconds(1);
  }
}

signed long read_cnt(const int clock_pin, 
                     const int data_pin) {
  unsigned long count = 0;
  int i;
  
  while( sizecvt(digitalRead(data_pin)) ) { 
    delay(1); 
  };
  
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(1);
  
  for(i=0;i<24; i++) {
    digitalWrite(clock_pin, HIGH);
    delayMicroseconds(1);
    count = count << 1;
    if ( sizecvt(digitalRead(data_pin)) > 0 )  { count++; }
    digitalWrite(clock_pin, LOW);
    delayMicroseconds(1);
  }
  
  digitalWrite(clock_pin, HIGH);
  delayMicroseconds(1);
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(1);
  
  if (count & 0x800000) {
    count |= (unsigned long) ~0xffffff;
  }
  
  // If above 0xFFFFFFFF / 2, then consider it negative
  signed long signedcount = count > 0x7FFFFFFF ? 
    - 0xFFFFFFFF + (signed long) count : 
    (signed long) count;
  
  // if things are broken this will show actual data
  return (signedcount);
}

int main(int argc, char *argv[]) {
  int iErr = 0;
  char cMode = 'V';
  FILE *f; 
  
  // Set up wiringPi
  iErr = wiringPiSetup();
  if (iErr == -1 && strcmp(&cMode, "V") == 0 ) {
    printf("ERROR : Failed to init WiringPi %d\n", iErr);
  }
  
  setHighPri();
  
  setup_gpio(clock_pin0, data_pin0);
#if BI_HX711
  setup_gpio(clock_pin1, data_pin1);
#endif
  
  set_gain(0, clock_pin0, data_pin0); 
#if BI_HX711
  set_gain(0, clock_pin1, data_pin1); 
#endif
  
  reset_converter(clock_pin0); 
#if BI_HX711
  reset_converter(clock_pin1); 
#endif
  
  // Open file for writing
  f = fopen(argv[1], "w");
  if (f == NULL) {
    printf("Error opening file!\n");
    exit(1);
  }
#if BI_HX711
  printf("# NOTE: this is a bi-load cell feeder.\n"); 
  printf("real, weight0, weight1\n"); 
  fprintf(f, "# NOTE: this is a bi-load cell feeder.\n"); 
  fprintf(f, "# real, weight0, weight1\n"); 
#else
  printf("# real, weight0\n"); 
  fprintf(f, "# real, weight0\n"); 
#endif
  fclose(f); 
  
  while (1) { 
    size_t bufsize=32; 
    
    char * realweight = (char *) malloc(bufsize * sizeof(char)); 
    printf("# Enter the real weight, then hit enter (Ctrl+C to exit): "); 
    getline(&realweight, &bufsize, stdin); 
    // Remove trailing whitespace
    realweight[strcspn(realweight, "\n")] = 0;
    
    for (int nsample = 0; nsample<N_RAW_POINTS; nsample++) { 
    
      // Get read and transform to grams
      signed long reading0 = read_cnt(clock_pin0, data_pin0); 
#if BI_HX711
      signed long reading1 = read_cnt(clock_pin1, data_pin1); 
#endif 
      
      f = fopen(argv[1], "a");
#if BI_HX711
      printf("%s, %ld, %ld\n", realweight, reading0, reading1); 
      fprintf(f, "%s, %ld, %ld\n", realweight, reading0, reading1); 
#else 
      printf("%s, %ld\n", realweight, reading0); 
      fprintf(f, "%s, %ld\n", realweight, reading0); 
#endif
      fclose(f); 
    
      nsample++; 
      usleep(UPDATE_HX711); 
    }
    
  }
  
  fclose(f); 

  return 0; 
}
