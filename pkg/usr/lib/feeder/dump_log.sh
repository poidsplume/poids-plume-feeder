#!/bin/bash

# Import settings
source /etc/feeder/feeder.conf 

# Dump logs of last hour
export WLOG="system.log"
journalctl --system --since "1 hour ago" | wlog

