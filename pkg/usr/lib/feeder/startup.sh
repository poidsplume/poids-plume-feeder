#!/bin/bash 
# Startup script for the feeder
# 

# Turn off power LED 
#   (taken from: 
# https://www.jeffgeerling.com/blogs/jeff-geerling/controlling-pwr-act-leds-raspberry-pi
# 
# 

source /etc/feeder/feeder.conf

# Get file to log to 
export WLOG="startup.log"

# Initialize a flag that tells us whether we will need to reboot after startup
REBOOT_NEEDED=0

# Disable hdmi port
if [ "$NOHDMI" -eq "1" ]; then 
  echo "Disabling HDMI port..." | wlog
  /opt/vc/bin/tvservice -o | wlog
  echo "Disabling video output..." | wlog
  vcgencmd display_power 0 | wlog
fi

# Disable bluetooth
if [ "$NOBLUETOOTH" -eq "1" ]; then 
  echo "Disabling bluetooth..." | wlog
  systemctl stop hciuart | wlog
  systemctl disable hciuart | wlog

  MODP_FILE="/etc/modprobe.d/bluetooth_blacklist.conf"
  if [ ! -e "$MODP_FILE" ]; then 
    echo "Blacklisting bluetooth modules..." | wlog 
    { 
      echo "# Blacklist bluetooth-related modules as we do not" \
           "use bluetooth" 
      echo "blacklist btbcm"     
      echo "blacklist hci_uart " 
    } >> "$MODP_FILE"
    
    if [ "$(grep -Ec "^dtoverlay=disable-bt" /boot/config.txt)" -eq "0" ]; then 
      echo "dtoverlay=disable-bt" >> /boot/config.txt
    fi
    REBOOT_NEEDED=1
  fi
fi

if [ "$NOUART" -eq "1" ]; then 
  echo "Disabling uart (WARNING: not implemented yet)" | wlog 
fi

# Turn swap off as we will never use that much memory
if [ "$NOSWAP" -eq "1" ]; then 
  echo "Turning off swap" | wlog
  dphys-swapfile swapoff | wlog
fi

# Set partitions to writeback mode so we don't write on the SD card so much 
if [ "$FS_WRITEBACK" -eq "1" ]; then 
  writeback_enabled="$(tune2fs -l /dev/mmcblk0p2 | grep "Default mount" | \
                        grep -c "journal_data_writeback")"
  if [ "$writeback_enabled" == "0" ]; then 
    echo "Enabling writeback" | wlog
    tune2fs -o journal_data_writeback /dev/mmcblk0p2 | wlog
    REBOOT_NEEDED=1
  fi 
fi

# Disable triggerhappy 
# echo "Disabling triggerhappy daemon..." | wlog
# systemctl stop triggerhappy.service | wlog
# systemctl disable triggerhappy.service | wlog
# systemctl disable triggerhappy.socket | wlog

# Update apt cache 
# apt-get update 

# Remove some unneeded packages
# for pkg in plymouth libplymouth4 bluez; do 
#   if [ "$(dpkg -l | grep ii | grep -c $pkg)" -gt 0 ]; then 
#     echo "Removing $pkg..." | wlog
#     apt-get -y remove $pkg | wlog
#     sleep 2 # make sure apt lock is released
#   fi
#   apt-get autoremove -y | wlog
# done

# Install extra software if needed
# for pkg in bc wiringpi i2c-tools r-base-core time dialog; do 
#   if [ "$( dpkg-query -W -f='${Status} \n' "$pkg" 2>/dev/null | \
#              grep -c "installed" )" -eq 0 ]; then 
#     echo "Installing $pkg..." | wlog
#     apt-get -y --no-install-recommends install $pkg | wlog
#     sleep 2 # make sure apt lock is released
#   fi
# done

# Enable i2c if not already done 
# This function comes from raspi-config*
echo "Enabling i2c..." | wlog
if [ "$(grep -Ec "^dtparam=i2c_arm=on" /boot/config.txt)" == "0" ]; then 
  echo "dtparam=i2c_arm=on" >> /boot/config.txt
  if [ "$(grep -c "^i2c-dev" /etc/modules)" -lt 1 ]; then 
    echo "i2c-dev" >> /etc/modules
  fi
  REBOOT_NEEDED=1
fi

# Set up timezone and ntp sync
echo "Setting up time options..."
timedatectl set-ntp True | wlog
timedatectl set-timezone "$FEEDERTZ" | wlog

# Make sure that systemd services are enabled at boot time. 
feederctl enable 

# Update binaries if required
if [ "$RECOMPILE" -gt 0 ]; then 
  echo "Recompiling at boot time..."
  feederctl compile
fi 

if [ "$REBOOT_NEEDED" -eq "1" ]; then 
  echo "Reboot needed, rebooting now !" | wlog
  reboot 
fi

# Disable system LED if option is set. We do this at the end of the startup 
# sequence so we know it's done. 
if [ "$NOLED" -eq "1" ]; then 
  echo "Turning off system LED (option NOLED=$NOLED)" | wlog
  
  # Set the Pi Zero ACT LED trigger to 'none' & turn off LED
  echo none > /sys/class/leds/led0/trigger 
  echo 0 > /sys/class/leds/led0/brightness 
  
fi

echo "Startup complete, have fun!" | wlog 

exit 0 
