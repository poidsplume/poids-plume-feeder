
// Required otherwise a warning is printed
#define _DEFAULT_SOURCE
#include <unistd.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// Status of weight 
#define UP 1
#define DOWN 0

// Step to launch camera
const double MINSTEP = 5.0; // Jump of five grams

// Number of observed deviations to start/stop camera
const int NDEVS = 3; 

// Limits on weight 
const double MAXWGT = HX711_MAXWGT;  // max weight in g
const double MINWGT = HX711_MINWGT;  // min weight in g

// Number of maximum time spent in UP phase 
const int TIMEOUT = 360; 

// Function declarations
double readval(char* file); 

int main(int argc, char *argv[]) {
  
  int devcount = 0; // Count of deviations
  int current_time = 0; 
  int status = DOWN; 
  int newstatus = UP; 
  
  while (1) { 
    
    // Read values 
    double w = readval(argv[1]); 
    
    // Reference weight (long-term average)
    double refw = readval(argv[2]); 
    
    // Detect if we are deviating from long-term average
    double deviation = fabs(w - refw); 
    
    if ( (status == DOWN && deviation > MINSTEP) || 
         (status == UP   && deviation < MINSTEP) ) { 
      devcount++; 
      printf("Deviation observed %02d/%02d\n", devcount, NDEVS); 
    } else { 
      // Set devcount back to zero as they need to be consecutive
      devcount = 0; 
    }
    
    // We need to change status
    if ( devcount == NDEVS ) { 
      newstatus = status == DOWN ? UP : DOWN; 
      
      printf("Change detected ! (%.3f) [%d -> %d]\n", deviation, status, newstatus); 
      if ( status == DOWN ) { 
        system(argv[3]); // Set camera up 
      } 
      
      current_time = 0; 
      status = newstatus; 
      devcount = 0; 
    } 
    
    // Force status back to DOWN if we reached timeout
    if ( status == UP && current_time == TIMEOUT ) {
      status = DOWN; 
      devcount = 0; 
    }
    
    current_time++; 

    usleep(UPDATE_HX711+1000); 
  }
  
}

double readval(char* file) {
  double d; 
  FILE *inp; 
  inp = fopen(file, "r"); 
  fscanf(inp, "%lf", &d); 
  fclose(inp); 
  return d; 
}
