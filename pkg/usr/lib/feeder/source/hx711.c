/*
 *     Copyright (2019), the Poids-Plume project (Alexandre Génin)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/* 
 * This code is derived from the wiringPi adapation of gurov's code 
 * available there: https://github.com/ggurov/hx711
 */

/* 
 * gurov was here, use this code, or don't, whatever, I don't care. If 
 * you see a giant bug with a billion legs, please let me know so it can 
 * be squashed
*/

// Avoids a warning with usleep 
// https://stackoverflow.com/questions/10053788/implicit-declaration-of-function-usleep
#define _DEFAULT_SOURCE

#include <stdint.h>
#include <sys/types.h>

#include <stdio.h>
#include <sched.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <math.h>

// Time between samples (microseconds)
#include <unistd.h>

// Parameters
const int data_pin0  = HX711_0_PIN;
const int clock_pin0 = HX711_0_CLK;

#if BI_HX711
const int data_pin1  = HX711_1_PIN;
const int clock_pin1 = HX711_1_CLK;
#endif 

const double wgt_offset = WEIGHTOFFSET;
const double wgt_coef0  = WEIGHTCOEF0; 

#if BI_HX711
const double wgt_coef1  = WEIGHTCOEF1; 
#else 
const double wgt_coef1  = 0; 
#endif

const double MAXDIFF    = HX711_MAXDIFF; // max diff in g
const double MAXWGT     = HX711_MAXWGT;  // max weight in g
const double MINWGT     = HX711_MINWGT;  // min weight in g
const int RESET_EVERY   = 500; 

// 
// Function that are common to all hx711 readers
// 

void setHighPri (void) {
  struct sched_param sched;
  memset (&sched, 0, sizeof(sched)) ;
  
  sched.sched_priority = 10 ;
  if ( sched_setscheduler(0, SCHED_FIFO, &sched) ) { 
    printf("Warning: Unable to set high priority\n");
  }
}

static uint8_t sizecvt(const int read) {
  /* digitalRead() and friends from wiringpi are defined as returning a 
    * value < 256. However, they are returned as int() types. This is a 
    * safety function 
    */
  if (read > 255 || read < 0) {
      printf("Invalid data from wiringPi library\n");
      exit(EXIT_FAILURE);
  }
  return (uint8_t)read;
}

void power_down_hx711(int clock_pin) {
  digitalWrite(clock_pin, HIGH);
}

void setup_gpio(int clock_pin, int data_pin) {
  pinMode(clock_pin, OUTPUT);
  pinMode(data_pin, INPUT);
  digitalWrite(clock_pin, LOW);
}

void reset_converter(int clock_pin) {
  digitalWrite(clock_pin, HIGH);
  delayMicroseconds(60);
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(60);
}

void set_gain(int r, int clock_pin, int data_pin) {
  int i;

  // r = 0 - 128 gain ch a
  // r = 1 - 32  gain ch b
  // r = 2 - 63  gain ch a

  while( sizecvt(digitalRead(data_pin)) ) { 
  };

  for (i=0;i<24+r;i++) {
    digitalWrite(clock_pin, HIGH);
    delayMicroseconds(1);
    digitalWrite(clock_pin, LOW);
    delayMicroseconds(1);
  }
}

signed long read_cnt(const int clock_pin, 
                     const int data_pin) {
  unsigned long count = 0;
  int i;
  
  while( sizecvt(digitalRead(data_pin)) ) { 
    delay(1); 
  };
  
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(1);
  
  for(i=0;i<24; i++) {
    digitalWrite(clock_pin, HIGH);
    delayMicroseconds(1);
    count = count << 1;
    if ( sizecvt(digitalRead(data_pin)) > 0 )  { count++; }
    digitalWrite(clock_pin, LOW);
    delayMicroseconds(1);
  }
  
  digitalWrite(clock_pin, HIGH);
  delayMicroseconds(1);
  digitalWrite(clock_pin, LOW);
  delayMicroseconds(1);
  
  if (count & 0x800000) {
    count |= (unsigned long) ~0xffffff;
  }
  
  // If above 0xFFFFFFFF / 2, then consider it negative
  signed long signedcount = count > 0x7FFFFFFF ? 
    - 0xFFFFFFFF + (signed long) count : 
    (signed long) count;
  
  // if things are broken this will show actual data
  return (signedcount);
}

int main(int argc, char *argv[]) {
  int iErr = 0;
  char cMode = 'V';
  double weight_g = 0.0;
  double oldweight = 0.0; 

  // Set up wiringPi
  iErr = wiringPiSetup();
  if (iErr == -1 && strcmp(&cMode, "V") == 0 ) {
    printf("ERROR : Failed to init WiringPi %d\n", iErr);
  }
  
  setHighPri();
  
  setup_gpio(clock_pin0, data_pin0);
#if BI_HX711
  setup_gpio(clock_pin1, data_pin1);
#endif
  
  set_gain(0, clock_pin0, data_pin0); 
#if BI_HX711
  set_gain(0, clock_pin1, data_pin1); 
#endif
  
  reset_converter(clock_pin0); 
#if BI_HX711
  reset_converter(clock_pin1); 
#endif
  
  // get a single sample
  int nsample = 0; 
  while (1) { 
    
    // Get read and transform to grams
    signed long reading0 = read_cnt(clock_pin0, data_pin0); 
#if BI_HX711
    signed long reading1 = read_cnt(clock_pin1, data_pin1); 
#else 
    signed long reading1 = 0; 
#endif
    
    
    weight_g = wgt_offset + 
                 (double) reading0 * wgt_coef0 + 
                 (double) reading1 * wgt_coef1;
    
#ifdef LOGDEBUG
    printf("Debug[weight]: %ld,%ld -> %f (coef/offset: %f+%f/%f)\n", 
           reading0, reading1, weight_g, wgt_coef0, wgt_coef1, 
           wgt_offset); 
#endif
    
    nsample++; 
    if ( nsample >= RESET_EVERY ) { 
#ifdef LOGDEBUG
      printf("Resetting hx711...\n"); 
#endif
      reset_converter(clock_pin0); // Try to reset converter regularly
#if BI_HX711
      reset_converter(clock_pin1); // Try to reset converter regularly
#endif
      nsample = 0; 
    }
    
    double valid_weight = 0.0; 
    if ( fabs( weight_g - oldweight ) > MAXDIFF || 
         weight_g < MINWGT || 
         weight_g > MAXWGT ) { 
      // Keep old weight
#ifdef LOGDEBUG
      printf("Invalid weight: %f [raw value: %ld,%ld]\n", 
             weight_g, reading0, reading1); 
#endif
      valid_weight = oldweight; 
      reset_converter(clock_pin0); 
#if BI_HX711
      reset_converter(clock_pin1); 
#endif
    } else { 
#ifdef LOGDEBUG
      printf("Valid weight: %f [raw value: %ld,%ld]\n", 
             weight_g, reading0, reading1); 
#endif
      valid_weight = weight_g; 
    }
    
    // Write to file given as first argument
    FILE * f; 
    f = fopen(argv[1], "w");
    if (f == NULL) {
      printf("Error opening file!\n");
      exit(1);
    }
    
    fprintf(f, "%f\n", valid_weight ); 
    fclose(f); 
    
#if BI_HX711
    // If we have a bi-HX711 feeder, then we also write the two weights 
    // separately. 
    // Write to file given as second argument
    f = fopen(argv[2], "w");
    if (f == NULL) {
      printf("Error opening file!\n");
      exit(1);
    }
    double weight0 = wgt_offset + (double) reading0 * wgt_coef0; 
    fprintf(f, "%f\n", weight0 ); 
    fclose(f); 
    
    // Write to file given as second argument
    f = fopen(argv[3], "w");
    if (f == NULL) {
      printf("Error opening file!\n");
      exit(1);
    }
    double weight1 = wgt_offset + (double) reading1 * wgt_coef1; 
    fprintf(f, "%f\n", weight1 ); 
    fclose(f); 
#endif
    
    // Put into old weight now for next reading
    oldweight = valid_weight; 
    
    usleep(UPDATE_HX711); 
  }
}
