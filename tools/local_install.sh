#!/bin/bash
# 
# This script will install the software when run directly on the raspberry pi
set -euo pipefail 

# Check if we are on a feeder 
if [ "$(which raspi-config 2>/dev/null | wc -l)" -eq 0 ]; then 
  echo "Erreur: ce système n'est pas un raspberry pi..."
  read -r -p "Appuyez sur une touche pour continuer"
  exit 1
fi 

# Check that we are root
if [ ! "$(id -u)" == 0 ]; then
  echo "Error: this command needs super-user privileges!"
  echo "Please use: sudo $0 $* instead of the provided command"
  exit 1
fi

# Package and install 
chmod +x ./pkg/DEBIAN/postinst
dpkg-deb --build pkg pkg.deb && sudo dpkg -i pkg.deb 

# Update things 
systemctl daemon-reload 
