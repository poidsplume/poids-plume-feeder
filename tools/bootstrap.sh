#!/bin/bash
# 
# This script will install the software on a raspbian vanilla install
set -euo pipefail 

# Check if we are root
if [ ! "$(id -u)" == 0 ]; then
  echo "Error: this command needs super-user privileges!"
  echo "Please use: sudo $0 $* instead of the provided command"
  exit 1
fi

# Default options 
CONFIRM=1
ORIGDIR="./pkg"
ROOT="/tmp/raspibootstrap"
SETUPONSERVER=0
PRINTUSAGE=0

if [ ! -d "$ORIGDIR" ]; then 
  echo "I could not find the software in the current directory. Aborting."
  exit 1
fi

# Handle options
while getopts "ysh" opt; do
  case ${opt} in
    y ) # process option a
      CONFIRM=0
      ;;
    s ) # process option s
      SETUPONSERVER=1
      ;;
    h )
      PRINTUSAGE=1
      ;;
    \? ) 
      PRINTUSAGE=1
      ;;
  esac
done
shift $((OPTIND -1))

# Print usage if asked for it
if [ "$PRINTUSAGE" -eq 1 ] || [ $# -lt 1 ]; then 
  echo "Usage:"
  echo "" 
  echo "$0 <boot partition> <root partition>"
  exit 0
fi

if [ "$(id -u)" -ne 0 ]; then 
  echo "I need root privileges to work"
  exit 1
fi

echo -n "Checking if partitions need to be unmounted... "
if [ "$(mount | grep -c "$ROOT")" -gt 0 ]; then 
  umount "$ROOT/boot"
  umount "$ROOT"
fi
echo "OK"

BOOTPART="$1"
if [ -z "$BOOTPART" ]; then 
  echo "You need to specify a target boot partition as argument."
  echo "Aborting."
  exit 1
fi
shift 

ROOTPART="$1"
if [ -z "$ROOTPART" ]; then 
  echo "You need to specify a target root partition as argument."
  echo "Aborting."
  exit 1
fi

# Default parameter values
DEFAULT_DATASERVER="transmit.poids-plume.fr"
DEFAULT_DATAUSER="incoming"
DEFAULT_DATASERVROOT="/home/incoming/feederdata"
DEFAULT_FEEDERTZ="Europe/Paris"
DEFAULT_WEIGHTOFFSET="0"
DEFAULT_WEIGHTCOEF0="1"
DEFAULT_WEIGHTCOEF1="1"
DEFAULT_BI_HX711="0"

# Ask for feeder parameters
echo ""
echo "Please enter feeder parameters"

# Ask for feeder name
read -r -p "Feeder name: " FEEDERNAME
if [ -z "$FEEDERNAME" ]; then 
  echo "Bad feeder name"
  exit 1 
fi
# Convert to lowercase
FEEDERNAME="$(echo "$FEEDERNAME" | tr '[:upper:]' '[:lower:]')"

# Ask for timezone 
read -r -e -p "Feeder time zone [$DEFAULT_FEEDERTZ]: " FEEDERTZ
FEEDERTZ="${FEEDERTZ:-$DEFAULT_FEEDERTZ}"

# Ask for the scale calibration params 
read -r -e -p "Scale calibration offset [$DEFAULT_WEIGHTOFFSET]: " WEIGHTOFFSET 
WEIGHTOFFSET="${WEIGHTOFFSET:-$DEFAULT_WEIGHTOFFSET}"
read -r -e -p "Scale calibration slope (HX711 #1) [$DEFAULT_WEIGHTCOEF0]: " WEIGHTCOEF0
WEIGHTCOEF0="${WEIGHTCOEF0:-$DEFAULT_WEIGHTCOEF0}"

# Ask if the feeder has two load cells
read -r -e -p "Feeder with two load cells ? (1/0) [$DEFAULT_BI_HX711] " BI_HX711
BI_HX711="${BI_HX711:-$DEFAULT_BI_HX711}"

if [ "$BI_HX711" == "1" ]; then 
  read -r -e -p "Scale calibration slope (HX711 #2) [$DEFAULT_WEIGHTCOEF1]: " WEIGHTCOEF1
  WEIGHTCOEF1="${WEIGHTCOEF1:-$DEFAULT_WEIGHTCOEF1}"
fi

read -r -e -p "Sync server[$DEFAULT_DATASERVER]: " DATASERVER
DATASERVER="${DATASERVER:-$DEFAULT_DATASERVER}"
read -r -e -p "Sync user[$DEFAULT_DATAUSER]: " DATAUSER
DATAUSER="${DATAUSER:-$DEFAULT_DATAUSER}"
read -r -e -p "Folder on sync server[$DEFAULT_DATASERVROOT]: " DATASERVROOT
DATASERVROOT="${DATASERVROOT:-$DEFAULT_DATASERVROOT}"
DATASERVDIR="$DATASERVROOT/$FEEDERNAME"
read -r -e -p "Extra compilation flags[]: " EXTRAFLAGS 

echo ""
echo ""
echo ""
echo "I will install the feeder software to ${ROOTPART%%p?}:"
echo "----------------------------------------------------------"
echo "Name: $FEEDERNAME"
echo "Time zone: $FEEDERTZ"
echo "Scale calibration offset: $WEIGHTOFFSET"
echo "Scale calibration slope: $WEIGHTCOEF0"
if [ "$BI_HX711" == "1" ]; then 
  echo "Scale calibration slope (HX711 #2): $WEIGHTCOEF1"
fi
echo "Sync server: $DATASERVER"
echo "Sync user: $DATAUSER"
echo "Folder on sync server: $DATASERVROOT/$FEEDERNAME"
echo "Extra compilation flags: $EXTRAFLAGS"
echo ""
if [ "$CONFIRM" -eq 1 ]; then 
  read -r -p "Do it ? (type 'yes' to do it) " yn 
  if [ -z "$yn" ] || [ "$yn" != "yes" ]; then 
    echo "Aborting"
    exit 1 
  fi
fi
echo ""

echo -n "Mounting root and boot partitions... "
mkdir -p "$ROOT"
mount "$ROOTPART" "$ROOT"
mount "$BOOTPART" "$ROOT/boot"
echo "OK"

if [ ! -e "$ROOT/etc/hostname" ] || [ ! -e "$ROOT/boot/config.txt" ]; then 
  echo "The mounted partitions do not look like a raspbian install."
  echo "Are you sure you selected the right partitions?"
  echo "Aborting."
  # umount "$ROOTPART"
  # umount "$BOOTPART"
  exit 1
fi

echo -n "Copying files... "
cp -r "$ORIGDIR/usr" "$ROOT"
cp -r "$ORIGDIR/etc" "$ROOT"
echo "OK"

echo -n "Setting up cron.d permissions... "
chown root:root "$ROOT"/etc/cron.d/{feeder_sync,feeder_logdump,feeder_data_cleanup}
chmod 700 "$ROOT"/etc/cron.d/{feeder_sync,feeder_logdump,feeder_data_cleanup}
echo "OK"

# Create the local.conf file 
echo -n "Creating local.conf file... " 
LOCALFILE="$ROOT/etc/feeder/local.conf" 
if [ -e "$LOCALFILE" ]; then 
  echo "" 
  echo -n "WARN: $LOCALFILE already exists... overwriting it! "
  rm "$LOCALFILE"
fi

touch "$LOCALFILE"
{ 
echo "# Generated by install script on $(date):" 
echo "FEEDERNAME=$FEEDERNAME"  
echo "FEEDERTZ=$FEEDERTZ"      
echo "BI_HX711=$BI_HX711"      
echo "WEIGHTCOEF0=$WEIGHTCOEF0"
if [ "$BI_HX711" == "1" ]; then 
  echo "WEIGHTCOEF1=$WEIGHTCOEF1" 
fi
echo "WEIGHTOFFSET=$WEIGHTOFFSET" 
echo "DATASERVER=$DATASERVER"   
echo "DATAUSER=$DATAUSER"       
echo "DATASERVDIR=$DATASERVDIR" 
echo "EXTRAFLAGS=$EXTRAFLAGS"   
} >> "$LOCALFILE"
echo "OK"

# Create hostname file 
echo -n "Setting hostname... "
echo "$FEEDERNAME" > "$ROOT/etc/hostname"
sed -i "s/raspberrypi/$FEEDERNAME/g" "$ROOT/etc/hosts"
echo "OK"

# Create ssh key 
echo -n "Generating ssh key... "
if [ -e "$ROOT/root/.ssh/" ]; then 
  echo ""
  echo -n "WARN: Key exists on local computer, replacing it with new one..."
  rm -rf "$ROOT/root/.ssh"
fi

# Cleanup 
rm -rf id_rsa id_rsa.pub
ssh-keygen -C "pi@$FEEDERNAME" -f id_rsa -N "" >/dev/null
mkdir -p "$ROOT/root/.ssh"
cp id_rsa id_rsa.pub "$ROOT/root/.ssh/"
echo "OK"

# Add the ssh key on the data-receiving server
if [ "$SETUPONSERVER" -eq 1 ]; then 
  echo -n "Adding key on server... "
  ssh "root@$DATASERVER" "cat >> /home/$DATAUSER/.ssh/authorized_keys" \
    < "id_rsa.pub"
  echo "OK"
else 
  echo "WARN: Not adding key on server - this feeder will be unable to send data" 
fi

# Set options in raspberry ssh to accept all server keys when connecting 
# to the host server. This prevents having the interactive 
# accept... yes/no on the first connection
echo -n "Setting ssh options on rpi... "
{
echo "Host $DATASERVER"            
echo "  StrictHostKeyChecking no"  
echo "  UserKnownHostsFile /dev/null" 
} >> "$ROOT/etc/ssh/ssh_config"
echo "OK" 

# Set time zone 
echo -n "Setting time zone... "
echo "$FEEDERTZ" > "$ROOT/etc/timezone"
echo "OK"

# Set boot options 
echo -n "Setting /boot/config.txt options... "
if [ "$(grep -c "install script" "$ROOT/boot/config.txt")" -gt 0 ]; then 
  echo "already added!"
else 
  { 
  echo "# Options added by install script on $(date)"
  echo "start_x=1"                        
  echo "gpu_mem=256"                      
  echo "disable_camera_led=1"             
  } >> "$ROOT/boot/config.txt"
  echo "OK"
fi

# Enable startup scripts
echo -n "Enabling systemd units... " 
for serv in feeder_startup feeder_logging feeder_recording; do 
  if [ ! -L "$ROOT/etc/systemd/system/multi-user.target.wants/$serv.service" ]; then 
    ln -s "$ROOT/etc/systemd/system/$serv.service" \
      "$ROOT/etc/systemd/system/multi-user.target.wants/$serv.service"
  fi
done
echo "OK" 

# Organize files 
CONFIGDIR="configs/$FEEDERNAME" 
mkdir -p "$CONFIGDIR"
mv id_rsa id_rsa.pub "$CONFIGDIR"
cp "$LOCALFILE" "$CONFIGDIR/local.conf"
touch "$CONFIGDIR/install_on_$(date)" 

# Make things ready server-side 
if [ "$SETUPONSERVER" -eq 1 ]; then 
  echo -n "Making directory on server... "
  ssh "root@$DATASERVER" "mkdir -p $DATASERVDIR && chown $DATAUSER:$DATAUSER $DATASERVDIR"
  echo "$DATASERVDIR"
fi

# Clean up
sync
umount "$ROOT/boot"
umount "$ROOT"
rmdir "$ROOT"

# Pause before continuing 
echo ""
echo "Success ! Type any key to continue" 
read -r -p ""


exit 0

