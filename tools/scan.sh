#!/bin/bash 
# 

# 
# Scan hosts on the network to which we can ssh (== connect on port 22)
# 

SSHPORT=22

function ssh_ok { 
  timeout 1s bash -c "true <> /dev/tcp/$1/$SSHPORT" &>/dev/null 
  code=$?
  if [ "$code" -eq 0 ]; then 
    echo 1
  else 
    echo 0
  fi
}

# Get current ip prefix
IP="$(ip route get 8.8.8.8 | grep -Po "src .* " | cut -d " " -f 2)"
IP_PREFIX="$(echo "$IP" | sed 's/ //g' | sed 's/[0-9]\+$//')"

echo "Recherche des hôtes sur le réseau courant (${IP_PREFIX}*)."
echo "Utilisez Ctrl^C pour quitter à tout moment"
for i in {1..255}; do 
  ip="${IP_PREFIX}$i"
  echo -n "."
  if [ "$(ssh_ok "$ip")" -gt 0 ]; then 
    echo ""
    # Host is ssh-accessible 
    echo "Mangeoire potentielle: $ip"
    echo "Utiliser ssh pi@$ip pour tenter d'y accéder"
  fi
done
