#!/bin/bash
# 

# 
# Write the disc image to the feeder memory card 
# 
set -euo pipefail 

# Check that we are root
if [ ! "$(id -u)" == 0 ]; then
  echo "Error: this command needs super-user privileges!"
  echo "Please use: sudo $0 $* instead of the provided command"
  exit 1
fi

# Check that the dev file is well-specified 
DEV_FILE="$1"
if [ -z "$DEV_FILE" ]; then 
  echo "You need to specify a target partition as argument."
  echo "Aborting."
  exit 1
fi

IMG_FILE="poidsplume_disc_current.img"

# Make sure the file is present on disk before writing to memory card
IMG_URL="https://static.poids-plume.fr/poidsplume_disc_current.img.xz"
if [ ! -e "$IMG_FILE" ]; then 
  echo "Image disque introuvable, téléchargement depuis le serveur PP"  
  wget "$IMG_URL" -O "${IMG_FILE}.xz" 
  echo "Décompression..."
  unxz --verbose "${IMG_FILE}.xz" 
fi

# Make sure there is a card in the reader 
if [ ! -e "$DEV_FILE" ]; then 
  echo "Périphérique introuvable ($DEV_FILE)"
fi

# Ask for confirmation
echo "L'image disque sera écrite sur le périphérique $DEV_FILE."
echo "Tout le contenu de la carte sera perdu!"
read -r -p "Continuer ? (tapez 'oui') " yn 
  if [ -z "$yn" ] || [ "$yn" != "oui" ]; then 
    echo "Vous n'avez pas tapé 'oui', arrêt."
    exit 1 
fi

# Write the image to the card 
echo "Ecriture de la carte mémoire..."
dd if="./$IMG_FILE" of="$DEV_FILE" status=progress bs=4M
echo "OK !"

# Pause before continuing
echo ""
echo "Taper sur une touche pour continuer"
read -r -p ""
