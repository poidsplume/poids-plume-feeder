﻿
<p align="center">
![Poids-Plume](https://poids-plume.fr/wp-content/uploads/2019/04/feeder-1.jpg) 
<h1>Poids-Plume - Feeder Software</h1>
</p>

This repository contains a set of scripts and C files used to run a bird feeder 
that takes videos of its visitors, along with their weights. The data can be 
transmitted to a distant server for later reuse. These scripts form the basis of 
the Poids-Plume (Featherweight in french) collaborative experiment, 
https://poids-plume.fr.

# Principle 

When the feeder starts, the raspbian system is booted, then the following 
sequence occurs: 

(1) If no connection is detected, an attempt is made at starting wifi-connect 
(the script `start-wifi-connect` is launched). This requires a [prior 
installation](#using-wifi-connect) of 
<a href="https://github.com/balena-io/wifi-connect" target="d"> wifi-connect</a> 
and its dependencies (NetworkManager, etc.). If this fails, the sequence 
continues, assuming the network is up. 

(2) Once the network setup has run, the feeder startup script is run 
(`/usr/lib/feeder/startup.sh`). This script disables devices on the board that 
the feeder will not use (e.g. bluetooth, HDMI output, LEDs), installs some 
software if missing, sets up time sync with ntp servers and compiles the logger 
readers. 

(3) Once the startup script has exited, the loggers are started. These are 
scripts that create files in `/run/feeder` which contain the sensor values. For 
example, the file `weight` is created in that folder, which contains the 
value read from the weight sensor. These files are updated more or less frequently
depending of the nature of the sensors (temperature for example is updated less 
frequently than weight). 

(4) Recording is then started. All files in `/run/feeder` are read and their 
values are copied into tabular files in subfolders of `/var/feeder` (following a 
year/month/day/hour directory structure). 

# Installation 

You should have a micro SD card to use in your Raspberry pi zero. Because the 
feeder may have to store many videos at once on the card, you probably want 
to have a relatively large SD card (a 16G card is needed to follow this 
tutorial). 

## Software installation (from a linux system)

### Installing the raspbian system on the card 

You need first to write the latest raspbian install on card. Raspbian can 
be downloaded from the <a href="https://www.raspberrypi.org/downloads/raspbian/" 
target="a">official website</a>. It is recommended to use 
<a href="https://downloads.raspberrypi.org/raspbian_lite_latest" target="b">
raspian lite</a>, which does not contain extra software that we will not use 
for this project, but in principle you should be able to use raspbian complete 
as well. 

Please note that if you don't feel like setting up the wifi by hand, 
a prepared disk image with user-friendly software to connect the feeder to a 
home network is available below (see section "Configuring WiFi"). 

After having downloaded the zip file and extracted it, you should have an image 
file `raspbian_lite_latest.img`. We need to write this file on the sd card. Plug 
your SD card into a card reader, then find out which block device it corresponds 
to (the file in `/dev/*`). You can use the output of the command `lsblk -f` to 
find this out. On my system for example, `lsblk -f` outputs the following: 

```
NAME        FSTYPE LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINT
sda                                                                           
└─sda1      ext4          70b19528-ec25-11e9-81b4-2a2ae2dbcce4  250,4G    75% /home
sdb                                                                           
├─sdb1      ext4   BOOT   ad29989e-8002-4e9f-957b-b51859fabe85   48,2M    54% /boot
├─sdb2      ext4   ROOT   7d307288-ec25-11e9-81b4-2a2ae2dbcce4     15G    67% /
└─sdb3      ntfs   WIN    3B9E94F3003C8330                                    
mmcblk0                                                                       
└─mmcblk0p1 vfat   SDCARD 1564-A412                                           
```

We can see that my computer has three disks attached: `sda` and `sdb` are 
internal hard drives (with one and three partitions, respectively). `mmcblk0` is 
my sd card (with one partition): we will write the image file to it. 

*Make sure you double check the disk you are writing on! Choosing the wrong disk 
may result in data loss and an unusable computer.*

```bash
sudo dd if=./raspbian_lite_latest.img of=/dev/mmcblk0 bs=4M status=progress
```

where `/dev/mmcblk0` should be replaced by your memory card device. Once this 
is done, you should have now two partitions available: `/dev/mmcblk0p1` and 
`/dev/mmcblk0p2`, which correspond to the raspbian boot and root partitions 
(respectively). 

Again, we can use `lsblk -f` to check that the image has been written correctly.
After writing the image, `lsblk -f` outputs the following:

```
NAME        FSTYPE LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINT
sda                                                                           
└─sda1      ext4          70b19528-ec25-11e9-81b4-2a2ae2dbcce4  250,4G    75% /home
sdb                                                                           
├─sdb1      ext4   BOOT   ad29989e-8002-4e9f-957b-b51859fabe85   48,2M    54% /boot
├─sdb2      ext4   ROOT   7d307288-ec25-11e9-81b4-2a2ae2dbcce4     15G    67% /
└─sdb3      ntfs   WIN    3B9E94F3003C8330                                    
mmcblk0
├─mmcblk0p1 vfat   boot   F236-A0E1
└─mmcblk0p2 ext4   rootfs 5319470a-0b28-43e1-bd31-ae94a8b922c0
```

Now the SD card has two partitions, one labelled `boot` and the other `rootfs`, 
which correspond the boot and root partitions of a raspbian system. 

### Installing the feeder software

Now that we have a raspbian system on the card, we need to install the feeder 
software. You can do so by downloading the files available from this 
<a href="https://gitlab.com/poidsplume/poids-plume-feeder" target='c'>
repository</a> using git: 

```bash 
git clone "https://gitlab.com/poidsplume/poids-plume-feeder" 
```

This will download the necessary software into a sub-directory called 
`poids-plume-feeder`. Install the feeder software on the card using: 

```bash 
cd poids-plume-feeder
sudo ./bootstrap.sh /dev/mmcblk0p1 /dev/mmcblk0p2
```

The script will ask you a few questions regarding your feeder, including its 
name, the time zone it will be in, the calibration parameters (offset and 
slope), see 'Load Cell Calibration' below), the server to send the data to and 
the connection parameters. You can also add extra compilation flags (e.g. 
`-DLOGDEBUG`, which will make the loggers print a lot of debug information in 
the system logs). 

If the server is accessible, it will try to install your ssh public key there to 
make sure the data can be transmitted. If this fails, it will be up to you to do 
this step (you will have to coordinate with the server administrators for this, 
refer to the section 'Sending Data' below).

SSH access to the feeder can be enabled by creating the file `/boot/ssh` on your 
pi. When on, you can connect to the Pi using `ssh pi@<pi_ip>` (where the ip is 
that of the Pi, e.g. `192.168.1.10`). 

### Configuring Wifi

#### Manual configuration

At this stage, you may want to set up the wifi on your Pi. The wifi can be set 
up using the command `wpa_passphrase` on a linux computer. Get your access point 
SSID ('name') and passphrase, then run the following command: 

```bash
wpa_passphrase AP_SSID mypasswd # replace as appropriate
```
you will then get the following output: 

```
network={
        ssid="AP_SSID"
        #psk="mypasswd"
        psk=27f53882f2ad05bbd963d21fa0108802084da11b5372ffe1a327521581d80722
}
```

You need to copy these lines, along with a few others into the following file on
your Pi: `/boot/wpa_supplicant.conf`. The file content should look like this 
(for a feeder located in France): 

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=FR
network={
        ssid="AP_SSID"
        #psk="mypasswd"
        psk=27f53882f2ad05bbd963d21fa0108802084da11b5372ffe1a327521581d80722
}
```

On next boot, the Pi should pick up the parameters and use them to connect to 
your Wifi network. You can remove the line `psk="mypasswd"` which is a comment 
if you do not want your Wifi key to be stored in clear text. 

Once you have done these steps, you can remove the SD card from your computer 
and put it into your Raspberry Pi. If all loggers/cameras are connected 
correctly, your feeder should be quickly up and running. 

#### Using Wifi-connect 

Installing WiFi-connect can be tricky as it requires an alternative network 
connection on top of the Pi wifi connection. To simplify the process, a raspbian 
image with Wifi-connect already installed is available. This will allow you to 
set up the wifi without having to edit files on the SD card: the pi will create 
an access point (with ID `WiFi-connect`), to which you can connect to enter the 
Wifi credentials. 

Download and extract using the following commands: 

```
wget "https://static.poids-plume.fr/2020-07-05_rev3_raspbian-buster-lite%2Bwifi-connect.img.xz"
tar -xf "2020-07-05_rev3_raspbian-buster-lite+wifi-connect.img.xz"
```

This will create a 16G `.img` file (!) that you can write on an SD card (this 
requires a card of size 16G, you will need to resize the partitions if the card 
has a different capacity). You can then follow the tutorial above to install the 
feeder software normally, using the new image file instead of 
`raspian_lite_latest.img`.


# Load Cell Calibration 

The feeder uses a load cell to weigh birds and turn on the camera when 
a bird is at the feeder. The load cell needs to be calibrated prior to use 
so that raw readings can be converted into grams. 

A small utility is included with the scripts from this repository to do so, 
`/usr/bin/hx711calib`. This script will pause the feeder logging/recording 
functions, then read raw values from the hx711 amplifier (which measures 
voltage at the load cell). Using objects with known weights, we can convert
raw readings into grams. 

On a command line on the Pi, run `hx711calib`. This will ask you for the real 
weight currently on the feeder perch, then take measurements of the raw values. 
The feeder will then calibrate itself based on the measurements you provided, 
and save the calibration parameters if things went smoothly. 

Calibration data is written into `/etc/feeder/local.conf`, along with a few 
other parameters. You can import previous calibration results by replacing 
the values in this file. 

# Sending data 

By default, the feeder will try to send data to the server you configured at 
install time (default server: `transmit.poids-plume.fr`). This is done using 
`rsync` over an ssh connection. 

To be able to transfer data, you need to get your ssh public key accepted 
by the administrators. Please send you public key to `bureau@poids-plume.fr`, 
along with the details of your installation. 

The file `id_rsa.pub` containing the public key is in `./configs/<feedername>` 
(in the same folder as `bootstrap.sh`), or in `/root/.ssh/id_rsa.pub` on the 
raspberry pi SD card. 








