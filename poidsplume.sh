#!/bin/bash

HEIGHT=15
WIDTH=80
CHOICE_HEIGHT=4
BACKTITLE="Poids-Plume"
TITLE="Outils Poids-Plume"
MENU="Actions possible"

OPTIONS=(1 "Chercher une mangeoire sur le réseau" 
         2 "Ecrire une image disque"
         3 "Installer le logiciel sur une carte SD"
         4 "Installer le logiciel sur le système courant" 
         5 "Quitter")

# Check that we have a connection to internet
ping -c1 "poids-plume.fr" &> /dev/null
# shellcheck disable=SC2181 # not sure if the alternative would work
if [ "$?" -ne "0" ]; then 
  echo "Erreur: pas d'accès internet. Ces outils nécessitent internet pour marcher."
  exit 1
fi

DOQUIT=0
while [ "$DOQUIT" == "0" ]; do 
  
  ACTION="quit"
  
  CHOICE=$(dialog --clear \
                  --backtitle "$BACKTITLE" \
                  --title "$TITLE" \
                  --menu "$MENU" \
                  $HEIGHT $WIDTH $CHOICE_HEIGHT \
                  "${OPTIONS[@]}" \
                  2>&1 >/dev/tty)
  
  clear
  case $CHOICE in
    1) ACTION="scan" ;;
    2) ACTION="write_image" ;;
    3) ACTION="bootstrap" ;;
    4) ACTION="local_install" ;;
    5) ACTION="quit" ;;
  esac
  
  if [ "$ACTION" == "scan" ]; then 
    ./tools/scan.sh 
  fi 

  if [ "$ACTION" == "write_image" ]; then 
    sudo ./tools/write_image.sh /dev/mmcblk0
  fi 

  if [ "$ACTION" == "bootstrap" ]; then 
    sudo ./tools/bootstrap.sh -s /dev/mmcblk0p1 /dev/mmcblk0p2
  fi 

  if [ "$ACTION" == "local_install" ]; then 
    sudo ./tools/local_install.sh 
  fi 
  
  if [ "$ACTION" == "quit" ]; then 
    DOQUIT=1
  fi
  
done 

exit 0
